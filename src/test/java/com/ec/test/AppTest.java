package com.ec.test;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class AppTest {
    @Test
    public void testApp() {

        assertThat(new App().currentDate(), containsString("2020"));
    }
}
