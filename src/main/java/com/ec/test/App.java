package com.ec.test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class App {
    public String currentDate() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").format(LocalDateTime.now());
    }
}
